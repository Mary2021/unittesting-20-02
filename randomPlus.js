const random = require('./random');

function randomPlus(number) {
    return random() + number;
}

module.exports = randomPlus;